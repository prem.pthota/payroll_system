package com.payroll.system;

@SuppressWarnings("serial")
public class SalaryEmployee extends Employee {

	private String employeePosition;

	public String getEmployeePosition() {
		return employeePosition;
	}

	public void setEmployeePosition(String employeePosition) {
		this.employeePosition = employeePosition;
	}

	public SalaryEmployee(Integer employeeId, String employeeName, String employeeJobType) {
		super(employeeId, employeeName, employeeJobType);

	}

	@Override
	public Double calculateGrossPay() {
		setGrossPay(employeePosition.equalsIgnoreCase("Staff") ? 50000.0 : 100000.0);
		setTax();
		setNetPay();
		setNetPercent();
		return getGrossPay();
	}

}
