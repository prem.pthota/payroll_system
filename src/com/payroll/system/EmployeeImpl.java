package com.payroll.system;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EmployeeImpl {

	List<Employee> employees = new ArrayList<Employee>();

	public void populateEmployee(Scanner scanner) {

		Integer id, items;
		String input, name, employeePosition;
		Double hoursWorked, hourlyRate, unitPrice;
		boolean isWrongAnswer;
		try {
			do {
				isWrongAnswer = true;
				System.out.println("Pls enter if you are a Hourly, Salary ,Commission Empolyee or Exit [H,S,C,E] ");
				input = scanner.next();
				scanner.nextLine();
				switch (input.toUpperCase()) {
				case "H":
					System.out.println("Pls Enter Employee ID");
					id = scanner.nextInt();
					System.out.println("Pls Enter Employee Name");
					name = scanner.next();
					System.out.println("Pls Enter Working Hours");
					hoursWorked = scanner.nextDouble();
					System.out.println("Pls Enter Rate of Hour");
					hourlyRate = scanner.nextDouble();
					HourlyEmployee hourlyEmployee = new HourlyEmployee(id, name, "H");
					hourlyEmployee.setHourlyRate(hourlyRate);
					hourlyEmployee.setHoursWorked(hoursWorked);
					hourlyEmployee.calculateGrossPay();
					employees.add(hourlyEmployee);
					break;
				case "S":
					System.out.println("Pls Enter Employee ID");
					id = scanner.nextInt();
					System.out.println("Pls Enter Employee Name");
					name = scanner.next();
					System.out.println("Pls Enter Employee Position Staff or executives");
					employeePosition = scanner.next();
					SalaryEmployee salaryEmployee = new SalaryEmployee(id, name, "S");
					salaryEmployee.setEmployeePosition(employeePosition);
					salaryEmployee.calculateGrossPay();
					employees.add(salaryEmployee);
					break;
				case "C":
					System.out.println("Pls Enter Employee ID");
					id = scanner.nextInt();
					System.out.println("Pls Enter Employee Name");
					name = scanner.next();
					System.out.println("Pls Enter no of Items");
					items = scanner.nextInt();
					System.out.println("Pls Enter Item per cost");
					unitPrice = scanner.nextDouble();
					CommissionEmployee commissionEmployee = new CommissionEmployee(id, name, "C");
					commissionEmployee.setItems(items);
					commissionEmployee.setUnitPrice(unitPrice);
					commissionEmployee.calculateGrossPay();
					employees.add(commissionEmployee);
					break;
				case "E":
					isWrongAnswer = false;
				default:
					isWrongAnswer = false;
					System.out.println("Unexpected Selection: " + input);
				}
			} while (isWrongAnswer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void selectEmployee(Scanner scanner) {
		Integer id;
		System.out.println("Pls Enter Employee ID ");
		id = scanner.nextInt();

		List<Employee> list = employees.stream().filter(emp -> emp.getEmployeeId().equals(id)).toList();
		if (!list.isEmpty()) {
			Employee employee = list.get(0);
			employee.displayEmployee();
		} else {
			System.out.println("Employee ID Not Found");
		}

	}

	public void saveEmployee() throws IOException {
		FileOutputStream fileOutputStream = new FileOutputStream("employees.txt");
		try (ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream)) {
			outputStream.writeObject(employees);
			outputStream.flush();
			outputStream.close();
			System.out.println("success");

		}
	}

	public void loadEmployee() throws IOException, ClassNotFoundException {
		FileInputStream fileInputStream = new FileInputStream("employees.txt");
		try (ObjectInputStream inputStream = new ObjectInputStream(fileInputStream)) {
			List<Employee> list = (List<Employee>) inputStream.readObject();
			employees.clear();
			employees.addAll(list);
			System.out.println(employees);
			inputStream.close();
		}
	}
}
