package com.payroll.system;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class Employee implements Serializable {

	private Integer employeeId;
	private String employeeName;
	private String employeeJobType;

	private Double grossPay;
	private Double netPay;
	private Double netPercent;
	private Double tax;

	public Employee() {

	}

	public Employee(Integer employeeId, String employeeName, String employeeJobType) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.employeeJobType = employeeJobType;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeJobType() {
		return employeeJobType;
	}

	public void setEmployeeJobType(String employeeJobType) {
		this.employeeJobType = employeeJobType;
	}

	public Double getGrossPay() {
		return grossPay;
	}

	public void setGrossPay(Double grossPay) {
		this.grossPay = roundOff(grossPay);
	}

	public Double getNetPay() {
		return netPay;
	}

	public void setNetPay() {
		this.netPay = roundOff((getGrossPay() - getTax()));
	}

	public Double getNetPercent() {
		return netPercent;
	}

	public void setNetPercent() {
		this.netPercent = roundOff((getNetPay() / getGrossPay()) * 100);
	}

	public Double getTax() {
		return tax;
	}

	public void setTax() {
		this.tax = roundOff((getGrossPay() * 0.2));
	}

	@Override
	public String toString() {
		return "{employeeId=" + employeeId + ", employeeName=" + employeeName + ", employeeJobType="
				+ employeeJobType + ", grossPay=" + grossPay + ", netPay=" + netPay + ", netPercent=" + netPercent
				+ ", tax=" + tax + "}";
	}

	public Double roundOff(Double value) {
		return Math.round(value * 100.0) / 100.0;
	}

	public void displayEmployee() {
		System.out.println("Employee ID: " + getEmployeeId());
		System.out.println("Employee Name: " + getEmployeeName());
		System.out.println("Employee Job Title: " + getEmployeeJobType());

		System.out.println("Gross Pay: $" + getGrossPay());
		System.out.println("Tax: $" + getTax());
		System.out.println("Net Pay: $" + getNetPay());
		System.out.println("Net Percent: $" + getNetPercent() + "%");

	}

	public abstract Double calculateGrossPay();
}
