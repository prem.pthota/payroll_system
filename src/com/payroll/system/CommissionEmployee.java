package com.payroll.system;

@SuppressWarnings("serial")
public class CommissionEmployee extends Employee {

	private Integer items;
	private Double unitPrice;

	public Integer getItems() {
		return items;
	}

	public void setItems(Integer items) {
		this.items = items;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public CommissionEmployee(Integer employeeId, String employeeName, String employeeJobType) {
		super(employeeId, employeeName, employeeJobType);

	}

	@Override
	public Double calculateGrossPay() {
		Double totalPrice = items * unitPrice;
		Double employeeCommission = (totalPrice / 100) * 50;
		setGrossPay(employeeCommission);
		setTax();
		setNetPay();
		setNetPercent();
		return getGrossPay();
	}
}
