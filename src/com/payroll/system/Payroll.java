package com.payroll.system;

import java.util.Scanner;

public class Payroll {

	public static void main(String[] args) {
		Payroll payroll = new Payroll();
		payroll.menu();
	}

	public void menu() {
		Scanner scanner = new Scanner(System.in);
		EmployeeImpl employeeImpl = new EmployeeImpl();
		boolean isWrongAnswer;

		Integer input;
		try {
			do {
				System.out.println("1.Populate Employee");
				System.out.println("2.Select Employee ");
				System.out.println("3.Save Employee");
				System.out.println("4.Load Employee ");
				System.out.println("5.Exit");
				isWrongAnswer = true;
				input = scanner.nextInt();
				switch (input) {
				case 1:
					employeeImpl.populateEmployee(scanner);
					break;
				case 2:
					employeeImpl.selectEmployee(scanner);
					break;
				case 3:
					employeeImpl.saveEmployee();
					break;
				case 4:
					employeeImpl.loadEmployee();
					break;
				case 5:
					isWrongAnswer = false;
				default:
					isWrongAnswer = false;
					System.out.println("Unexpected Selection: " + input);
				}

			} while (isWrongAnswer);
			scanner.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
