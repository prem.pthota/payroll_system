package com.payroll.system;

@SuppressWarnings("serial")
public class HourlyEmployee extends Employee {

	private Double hoursWorked;
	private Double hourlyRate;

	public Double getHoursWorked() {
		return hoursWorked;
	}

	public void setHoursWorked(Double hoursWorked) {
		this.hoursWorked = hoursWorked;
	}

	public double getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(Double hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public HourlyEmployee(Integer employeeId, String employeeName, String employeeType) {
		super(employeeId, employeeName, employeeType);

	}

	@Override
	public Double calculateGrossPay() {
		Double overtimeHours = hoursWorked > 40 ? hoursWorked - 40 : 0;
		Double regularHours = hoursWorked - overtimeHours;
		setGrossPay((regularHours * hourlyRate) + (overtimeHours * (hourlyRate * 1.5)));
		setTax();
		setNetPay();
		setNetPercent();
		return getGrossPay();

	}

}
